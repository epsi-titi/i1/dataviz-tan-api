import pandas as pd
import requests
import re
import os
from datetime import datetime
import shutil
import json


def fetch_stops():
    if os.path.exists('files/stops/clean_data_stops.csv'):
        # récupération du fichier en local
        with open('files/stops/clean_data_stops.json', 'r') as r:
            data_json = json.load(r)
            print(data_json)
    else:
        # récupération des données depuis l'API de Nantes
        # récupération de l'url
        url = 'https://data.nantesmetropole.fr/api/v2/catalog/datasets/244400404_tan-arrets/exports/json?limit=-1&offset=0&lang=fr&timezone=UTC'
        # envoi de la requête
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
            df = pd.DataFrame.from_dict(data)
            # nettoyage des données
            data_json = clean_data(df)
    return data_json


def check_validity_date():
    date = datetime.today().strftime('%d-%m-%Y')
    # vérifie si le fichier est à jour


def rename_file_stops(filename):
    # récupération d ela date du jour
    date = datetime.today().strftime('%d-%m-%Y')
    # déplacement du fichier dans le dossier des fichiers
    shutil.move(filename, 'files/stops/' + filename)
    newfilename = 'files/stops/' + 'data_stops' + '_' + date + '.csv'
    # renommage du fichier avec la date
    os.rename('files/stops/' + filename, newfilename)

    return newfilename


def clean_data(df):
    # suppression des colonnes inutiles
    df.drop(['stop_code', 'stop_desc', 'zone_id', 'stop_url', 'stop_timezone', 'wheelchair_boarding'], axis=1,
            inplace=True)
    # suppression des lignes vides
    df.dropna(subset=['stop_name', 'stop_coordinates'], inplace=True)
    # création d'une colonne longitude et latitudeà partir d ela colonne stop_coordinates
    df[['latitude', 'longitude']] = df['stop_coordinates'].apply(lambda x: pd.Series([x['lat'], x['lon']]))
    # supression de la colonne stop_coordinates
    df.drop(['stop_coordinates'], axis=1, inplace=True)
    # decode du nom des arrêts
    df['stop_name'].str.decode('utf-8')
    # remplacement des NaN par des chaînes de caractères vides
    df = df.fillna('')
    # modification du types des colonnes
    df = df.astype(str)
    # suppression des lignes dupliquées
    df.drop_duplicates(inplace=True)
    # sauvegarde des données dans un fichier json
    df.to_json('files/stops/clean_data_stops.json', index=True)
    # récupération des données au format json
    data_json = df.to_json(orient='records')
    return data_json
