
# API dataviz-tan


## Prérequis

    python 3.6
    pandas
    uvicorn
    fastpi

## Installation

    git clone https://gitlab.tdassise.com:8443/epsi/i1/dataviz-tan-api.git

## Lancer l'API

    uvicorn main:app --reload

## Lancer l'API avec Docker-compose

    docker-compose up -d

    
    
`http://localhost:8000`

# REST API

### Liste des arrêts

`GET /get-stops/`


     http://localhost:8000/get-stops/
