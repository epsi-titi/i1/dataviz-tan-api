from fastapi import FastAPI
import data.data as data

app = FastAPI()


@app.get("/get-stops")
async def get_stops():
    return data.fetch_stops()
