FROM python:3.9

COPY ../ /app
COPY ./requirements.txt /app

WORKDIR /app

RUN pip install -r requirements.txt

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]